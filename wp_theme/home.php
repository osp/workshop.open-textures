<?php
// home.php

require_once('textures.php');

get_header();

while(have_posts())
{
    the_post();
    echo '<div class="post-box">
    <div class="post-title">
    '.get_the_title().'
    </div>
    <div class="post-content">
    '.get_the_content().'
    </div>
    </div>';
}

$T = new Texture('/home/pierre/System/src/osp.workshop.open-textures', 
                'http://localhost/~pierre/404/wp-content/themes/404/secure_image.php');
echo '<div id="git-box">';
$T->write();
echo '</div>';

get_footer();
?>
