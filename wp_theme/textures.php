<?php
// textures.php


function walk_dir($path, $pat)
{
    $stop = false;

    /* get the absolute path and ensure it has a trailing slash */
    $path = realpath($path);
    if (substr($path, -1) !== DIRECTORY_SEPARATOR)
    $path .= DIRECTORY_SEPARATOR;

    $queue = array($path => 1);
    $done  = array();
    $index = 0;

    $ret_array = array();

    while(!empty($queue)) 
    {
        /* get one element from the queue */
        foreach($queue as $path => $unused) 
        {
            unset($queue[$path]);
            $done[$path] = null;
            break;
        }
        unset($unused);
        
        $dh = @opendir($path);
        if (!$dh) continue;
        while(($filename = readdir($dh)) !== false) 
        {
            
            /* dont recurse back up levels */
            if ($filename == '.' || $filename == '..')
                continue;
                
            $filename_base = $filename;
            $filename = $path . $filename;
                
            /* queue directories for later search */
            if (is_dir($filename)) 
            {
                /* ensure the path has a trailing slash */
                if (substr($filename, -1) !== DIRECTORY_SEPARATOR)
                    $filename .= DIRECTORY_SEPARATOR;
                
                /* check if we have already queued this path, or have done it */
                if (array_key_exists($filename, $queue) || array_key_exists($filename, $done))
                    continue;
                
                /* queue the file */
                $queue[$filename] = null;
            }
            else
            {
                if (preg_match($pat, $filename_base) === 1) 
                {
                    error_log($filename);
                    $ret_array[] = $filename;
                }
            }
        }
        closedir($dh);
    }
    
    return $ret_array;
}


function sort_by_time($a, $b)
{
    $ta = filemtime($a);
    $tb = filemtime($b);
    if($ta < $tb)
        return -1;
    if($ta > $tb)
        return 1;
    return 0;
}
                    
class Texture
{
    public function __construct($root, $web_srv)
    {
        $this->root = $root;
        $this->web_srv = $web_srv ;
        $this->collect_images();
    }
    
    public function collect_images()
    {
        $this->files = walk_dir($this->root, '/.*\.(jpeg|png|jpg)$/i');
        usort($this->files, sort_by_time);
    }
    
    function make_url($path)
    {
        $md = md5_file($path);
        return $this->web_srv . '?p=' . urlencode($path) . '&h=' . urlencode($md);
    }
    
    public function write($limit=12, $class='git_image', $before='', $after='')
    {
        $count = 0;
        foreach($this->files as $path)
        {
            if($count >= $limit)
            {
                break;
            }
            echo $before.'<img class="'.$class.'" src="'.$this->make_url($path).'" />'.$after;
            $count += 1;
        }
    }
}



?>