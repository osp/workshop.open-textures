<?php
// footer.php
?>

<div class="footer-box">
Zona (temporal, espacial, práctica y discursiva) abierta y radicalmente
transdisciplinar, transgeneracional y transexperta. Una sensibilidad basada en
el aprendizaje-de-por-vida y en el reconocimiento de los saberes locales, con
sus tiempos y sus ritmos. Un programa de facilitación de aprendizajes
de/sobre/en creación cultural desde la pluralidad de posibilidades y la
problemática de recogerlas en un marco común. Una zona de contacto en la que lo
todavía-no-encontrado cobra protagonismo, en la que todas las personas son tan
aprendientes como enseñantes, dependiendo del momento, el asunto o el lugar, por
ejemplo.
</div>
</body>
</html>