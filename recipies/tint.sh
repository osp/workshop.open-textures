# usage:
# bash tint.sh input color output 
#
# example:
# bash tint.sh myimage.jpg red mytintedimage.jpg

#first they take an image 
#then convert it to greyscale
#something with contrast
#save the file greyscale.jpg
convert "$1" -colorspace gray /tmp/greyscale.tif



#take greyscale.tif
#fill switch sets the current color
#color as an argument
#tint the image with the fill color
#value as argument, percentage
#specify the output file 
convert /tmp/greyscale.tif -fill "$2" -tint 100 "$3"
